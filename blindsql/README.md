# blind-inject
---
This repository contains a program that performs a blind SQL injection attack on the Web for Pentester 2 site.

Originally hosted at https://github.com/rlui94/blind-inject/